module task_manager

go 1.15

require (
	github.com/Azure/go-autorest/logger v0.2.0
	github.com/garyburd/redigo v1.6.2
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible // indirect
	github.com/prometheus/client_golang v1.9.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	go.mongodb.org/mongo-driver v1.4.5
	go.uber.org/zap v1.16.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/telegram-bot-api.v4 v4.6.4
	gopkg.in/yaml.v2 v2.4.0 // indirect
	honnef.co/go/tools v0.1.1 // indirect
)
