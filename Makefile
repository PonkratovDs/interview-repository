COMMIT?=$(shell git rev-parse --short HEAD)
BUILD_TIME?=$(shell date -u '+%Y-%m-%d_%H:%M:%S')

export GO111MODULE=on

.PHONY: build
build:
	@echo "-- building binary"
	go build \
		-ldflags "-X main.buildHash=${COMMIT} -X main.buildTime=${BUILD_TIME}" \
		-o ./bin/task_manager \
		./cmd/task_manager/main.go

.PHONY: test
test:
	@echo "-- run tests"
	go test -v -coverpkg=./... ./...

.PHONY: lint
lint:
	golangci-lint run -v \
	--disable-all \
	-E deadcode,errcheck,gosimple,govet,ineffassign,staticcheck,\
	structcheck,typecheck,unused,varcheck,bodyclose,errorlint,\
	exportloopref,gochecknoinits,gocognit,goconst,gocritic,\
	godot,gofmt,goimports,goheader,lll,prealloc

.PHONY: dc
dc:
	@echo "-- starting docker compose"
	docker-compose -f ./deployments/docker-compose.yml up

.PHONY: dcb
dcb:
	@echo "-- starting docker compose"
	go build \
		-ldflags "-X main.buildHash=${COMMIT} -X main.buildTime=${BUILD_TIME}" \
		-o ./bin/task_manager \
		./cmd/task_manager/main.go
	docker-compose -f ./deployments/docker-compose.yml up

.PHONY: deploy
deploy:
	@echo "-- starting deploy"
	scp bin/task_manager dponkratov@84.252.131.73:/home/dponkratov/task_manager/task_manager
	scp -r deployments dponkratov@84.252.131.73:/home/dponkratov/task_manager
	scp Makefile dponkratov@84.252.131.73:/home/dponkratov/task_manager/Makefile