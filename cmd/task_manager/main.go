package main

import (
	"context"
	"task_manager/pkg/handlers"
	"task_manager/pkg/items"

	"github.com/garyburd/redigo/redis"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"go.uber.org/zap"
	"gopkg.in/mgo.v2"

	_ "go.mongodb.org/mongo-driver/mongo"
)

var (
	timings = promauto.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "timing_hits",
			Help: "Per method timing",
		},
		[]string{"status", "path"}, // status - либо 200, либо 500, если есть ошибка. path - имя функции
	)
	hits = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "count_hits",
		},
		[]string{"status", "path"},
	)
)

func main() {
	zapLogger, _ := zap.NewProduction()
	defer func(zapLogger *zap.Logger) {
		err := zapLogger.Sync()
		if err != nil {
			panic(err)
		}
	}(zapLogger)
	logger := zapLogger.Sugar()
	// mongodb://mongodb:27017/
	sess, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		logger.Error("error while creating database MongoDB")
		return
	}
	redisConn, err := redis.DialURL("redis://user:@localhost:6379/0")
	// redisConn, err := redis.DialURL("redis://user:@redis:6379/0")
	if err != nil {
		logger.Error("error while creating database redis")
		return
	}
	defer redisConn.Close()
	dbHelper := items.NewDatabaseHelper(sess.DB("task_manager"))
	err = handlers.StartTaskBot(context.Background(), dbHelper, redisConn, logger, timings, hits)
	if err != nil {
		panic(err)
	}
}
