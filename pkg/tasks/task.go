package tasks

type Task struct {
	ID       uint64
	Owner    string
	Executor string
	Name     string
}

// type TaskID struct {
// 	ID       uint64
// 	TaskFlag bool
// }
