package handlers

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync/atomic"
	"task_manager/pkg/items"
	"task_manager/pkg/tasks"

	"github.com/garyburd/redigo/redis"
	"go.uber.org/zap"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

type BotHandler struct {
	repo   *items.ItemsRepo
	logger *zap.SugaredLogger
	taskID uint64
}

func NewBotHandler(db items.DatabaseHelper, rc redis.Conn, logger *zap.SugaredLogger) *BotHandler {
	var taskID uint64 = 1
	repo := items.NewItemsRepo(db, rc)
	id, err := repo.GetTaskID()
	if err == nil {
		taskID = id
	}
	return &BotHandler{
		repo:   repo,
		logger: logger,
		taskID: taskID,
	}
}

func (api *BotHandler) New(bot *tgbotapi.BotAPI, update *tgbotapi.Update, owner *tgbotapi.User, query string) error {
	name, err := getSplits(query, "/new ", "/new", "/new Создать задачу")
	if err != nil {
		api.logger.Errorf("error when executing a function named `New`, method getSplits: %w", err)
		return err
	}
	taskID := atomic.LoadUint64(&api.taskID)
	err = api.repo.AddNewTaskByID(&tasks.Task{
		ID:    taskID,
		Owner: owner.UserName,
		Name:  name,
	})
	if err != nil {
		api.logger.Errorf("error when executing a function named `New`, method AddNewTaskByID: %w", err)
		return err
	}
	message := fmt.Sprintf(`Задача "%s" создана, id=%d`, name, api.taskID)
	_, err = bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, message))
	if err != nil {
		api.logger.Errorf("error when executing a function named `New`, error when sending message by bot: %w", err)
		return err
	}
	atomic.AddUint64(&api.taskID, 1)
	err = api.repo.UpdateTaskID(taskID + 1)
	if err != nil {
		api.logger.Errorf("error when executing a function named `New`, method UpdateTaskID: %w", err)
		return err
	}
	return nil
}

func (api *BotHandler) Assign(bot *tgbotapi.BotAPI, update *tgbotapi.Update,
	executor *tgbotapi.User, query string) error {
	idStr, err := getSplits(query, "/assign_", "/assign", "/assign_10")
	if err != nil {
		api.logger.Errorf("error when executing a function named `Assign`, method getSplits: %w", err)
		return err
	}
	id, err := api.getIDAndCheckTask(idStr)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Assign`, method getIDAndCheckTask: %w", err)
		return err
	}
	task, err := api.repo.GetTaskByID(id)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Assign`, method GetTaskByID: %w", err)
		return err
	}
	oldExecutor := task.Executor
	if oldExecutor == "" {
		oldExecutor = task.Owner
	}
	task.Executor = executor.UserName
	err = api.repo.UpdateTask(task)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Assign`, method UpdateTask: %w", err)
		return err
	}
	message := fmt.Sprintf(`Задача "%s" назначена на `, task.Name)
	replyOwner := message + "@" + executor.UserName
	replyExecutor := message + "вас"
	if task.Owner == executor.UserName {
		_, err = bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, replyExecutor))
		if err != nil {
			api.logger.Errorf("error when executing a function named `Assign`, error when sending message by bot: %w", err)
			return err
		}
		return nil
	}
	chatID, err := api.repo.GetChatIDUsersByID(oldExecutor)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Assign`, method GetChatIDUsersByID: %w", err)
		return err
	}
	_, err = bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, replyExecutor))
	if err != nil {
		api.logger.Errorf("error when executing a function named `Assign`, error when sending message by bot: %w", err)
		return err
	}
	_, err = bot.Send(tgbotapi.NewMessage(chatID, replyOwner))
	if err != nil {
		api.logger.Errorf("error when executing a function named `Assign`, error when sending message by bot: %w", err)
		return err
	}
	return nil
}

func (api *BotHandler) Unassign(bot *tgbotapi.BotAPI, update *tgbotapi.Update,
	executor *tgbotapi.User, query string) error {
	idStr, err := getSplits(query, "/unassign_", "/unassign", "/unassign_10")
	if err != nil {
		api.logger.Errorf("error when executing a function named `Unassign`, method getSplits: %w", err)
		return err
	}
	id, err := api.getIDAndCheckTask(idStr)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Unassign`, method getIDAndCheckTask: %w", err)
		return err
	}
	task, err := api.repo.GetTaskByID(id)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Unassign`, method GetTaskByID: %w", err)
		return err
	}
	if task.Executor != executor.UserName {
		_, err = bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Задача не на вас"))
		if err != nil {
			api.logger.Errorf("error when executing a function named `Unassign`, error when sending message by bot: %w", err)
			return err
		}
		return nil
	}
	task.Executor = ""
	err = api.repo.UpdateTask(task)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Unassign`, method UpdateTask: %w", err)
		return err
	}
	replyExecutor := "Принято"
	replyOwner := fmt.Sprintf(`Задача "%s" осталась без исполнителя`, task.Name)
	chatIDOwner, err := api.repo.GetChatIDUsersByID(task.Owner)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Unassign`, method GetChatIDUsersByID: %w", err)
		return err
	}
	_, err = bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, replyExecutor))
	if err != nil {
		api.logger.Errorf("error when executing a function named `Unassign`, error when sending message by bot: %w", err)
		return err
	}
	_, err = bot.Send(tgbotapi.NewMessage(chatIDOwner, replyOwner))
	if err != nil {
		api.logger.Errorf("error when executing a function named `Unassign`, error when sending message by bot: %w", err)
		return err
	}
	return nil
}

func (api *BotHandler) Resolve(bot *tgbotapi.BotAPI, update *tgbotapi.Update,
	executor *tgbotapi.User, query string) error {
	idStr, err := getSplits(query, "/resolve_", "/resolve", "/resolve_10")
	if err != nil {
		api.logger.Errorf("error when executing a function named `Resolve`, method getSplits: %w", err)
		return err
	}
	id, err := api.getIDAndCheckTask(idStr)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Resolve`, method getIDAndCheckTask: %w", err)
		return err
	}
	task, err := api.repo.GetTaskByID(id)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Resolve`, method GetTaskByID: %w", err)
		return err
	}
	if task.Executor != executor.UserName {
		_, err = bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Вы не исполнитель этой задачи"))
		if err != nil {
			api.logger.Errorf("error when executing a function named `Resolve`, error when sending message by bot: %w", err)
			return err
		}
		return nil
	}
	message := fmt.Sprintf(`Задача "%s" выполнена`, task.Name)
	owner := task.Owner
	err = api.repo.DeleteTaskByID(id)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Resolve`, method DeleteTaskByID: %w", err)
		return err
	}
	replyOwner := message + " @" + executor.UserName
	replyExecutor := message
	chatIDOwner, err := api.repo.GetChatIDUsersByID(owner)
	if err != nil {
		api.logger.Errorf("error when executing a function named `Resolve`, method GetChatIDUsersByID: %w", err)
		return err
	}
	_, err = bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, replyExecutor))
	if err != nil {
		api.logger.Errorf("error when executing a function named `Resolve`, error when sending message by bot: %w", err)
		return err
	}
	_, err = bot.Send(tgbotapi.NewMessage(chatIDOwner, replyOwner))
	if err != nil {
		api.logger.Errorf("error when executing a function named `Resolve`, error when sending message by bot: %w", err)
		return err
	}
	return nil
}

func (api *BotHandler) My(bot *tgbotapi.BotAPI, update *tgbotapi.Update, user *tgbotapi.User) {
	result := ""
	intermediate := api.repo.GiveTasksWithCondition("executor", user.UserName)
	sort.SliceStable(intermediate, func(i, j int) bool {
		return intermediate[i].ID < intermediate[j].ID
	})
	for _, task := range intermediate {
		result += fmt.Sprintf("%d. %s by @%s\n/unassign_%d /resolve_%d\n", task.ID, task.Name, task.Owner, task.ID, task.ID)
	}
	if len(result) == 0 {
		_, err := bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "На вас не назначено задач"))
		if err != nil {
			api.logger.Errorf("error when executing a function named `My`, error when sending message by bot: %w", err)
			return
		}
		return
	}
	_, err := bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, result[:len(result)-1]))
	if err != nil {
		api.logger.Errorf("error when executing a function named `My`, error when sending message by bot: %w", err)
		return
	}
}

func (api *BotHandler) Owner(bot *tgbotapi.BotAPI, update *tgbotapi.Update, user *tgbotapi.User) {
	result := ""
	intermediate := api.repo.GiveTasksWithCondition("owner", user.UserName)
	sort.SliceStable(intermediate, func(i, j int) bool {
		return intermediate[i].ID < intermediate[j].ID
	})
	for _, task := range intermediate {
		if task.Executor == user.UserName {
			result += fmt.Sprintf("%d. %s by @%s\n/unassign_%d /resolve_%d\n", task.ID, task.Name, task.Owner, task.ID, task.ID)
		} else {
			result += fmt.Sprintf("%d. %s by @%s\n/assign_%d\n", task.ID, task.Name, task.Owner, task.ID)
		}
	}
	if len(result) == 0 {
		_, err := bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Вы не создали ни одной текущей задачи"))
		if err != nil {
			api.logger.Errorf("error when executing a function named `Owner`, error when sending message by bot: %w", err)
			return
		}
		return
	}
	_, err := bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, result[:len(result)-1]))
	if err != nil {
		api.logger.Errorf("error when executing a function named `Owner`, error when sending message by bot: %w", err)
		return
	}
}

func (api *BotHandler) Tasks(bot *tgbotapi.BotAPI, update *tgbotapi.Update, user *tgbotapi.User) {
	result := ""
	i := 0
	intermediate := api.repo.GiveTasks()
	limiter := len(intermediate) - 1
	if limiter == -1 {
		_, err := bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Нет задач"))
		if err != nil {
			api.logger.Errorf("error when executing a function named `Tasks`, error when sending message by bot: %w", err)
			return
		}
		return
	}
	sort.SliceStable(intermediate, func(i, j int) bool {
		return intermediate[i].ID < intermediate[j].ID
	})
	for _, task := range intermediate {
		if task.Name == "" {
			continue
		}
		result += fmt.Sprintf("%d. %s by @%s", task.ID, task.Name, task.Owner)
		if task.Executor == "" {
			result += fmt.Sprintf("\n/assign_%d\n", task.ID)
		} else {
			if task.Executor == user.UserName {
				result += fmt.Sprintf("\nassignee: я\n/unassign_%d /resolve_%d\n", task.ID, task.ID)
			} else {
				result += fmt.Sprintf("\nassignee: @%s\n", task.Executor)
			}
		}
		if i < limiter {
			result += "\n"
		}
		i++
	}
	if result == "" {
		_, err := bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Нет задач"))
		if err != nil {
			api.logger.Errorf("error when executing a function named `Tasks`, error when sending message by bot: %w", err)
			return
		}
		return
	}
	_, err := bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, result[:len(result)-1]))
	if err != nil {
		api.logger.Errorf("error when executing a function named `Tasks`, error when sending message by bot: %w", err)
		return
	}
}

func (api *BotHandler) getIDAndCheckTask(idStr string) (uint64, error) {
	id, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("typecast error: %w", err)
	}
	if !api.repo.CheckTaskByID(id) {
		return 0, fmt.Errorf("нет задачи по данному id %d", id)
	}
	return id, nil
}

func getSplits(query string, split string, command string, template string) (string, error) {
	splits := strings.Split(query, split)
	if len(splits) != 2 || splits[0] != "" {
		return "", fmt.Errorf("Invalid request for %s. You entered %s. Template: %s", command, query, template)
	}
	return splits[1], nil
}
