package handlers

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strings"
	"sync"
	"task_manager/pkg/items"
	"time"

	"github.com/garyburd/redigo/redis"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

var webhookURL = flag.String("webhookURL", "https://b56b23d11408.ngrok.io", "flag for webhook URL")

const (
	botToken = "1505026650:AAFZHWcKicWuAeaI0NrBChlAW0UKb5JH3J4"
	// webhookURL = "https://80900978d52f.ngrok.io"
)

type MiddlewareBot struct {
	sync.Mutex
	timings *prometheus.SummaryVec
	counts  *prometheus.CounterVec
	API     *BotHandler
}

func NewMiddlewareBot(timings *prometheus.SummaryVec, counts *prometheus.CounterVec, api *BotHandler) *MiddlewareBot {
	return &MiddlewareBot{timings: timings, counts: counts, API: api}
}

func StartTaskBot(ctx context.Context, db items.DatabaseHelper, rc redis.Conn, logger *zap.SugaredLogger,
	timings *prometheus.SummaryVec, counts *prometheus.CounterVec) error {
	bot, err := tgbotapi.NewBotAPI(botToken)
	if err != nil {
		return fmt.Errorf("error while creating a bot: %w", err)
	}
	*webhookURL = os.Getenv("webhookURL")
	// bot.Debug = true
	fmt.Printf("Authorized on account %s, ID bot %d\n", bot.Self.UserName, bot.Self.ID)
	_, err = bot.SetWebhook(tgbotapi.NewWebhook(*webhookURL))
	fmt.Println(1, *webhookURL)
	if err != nil {
		return fmt.Errorf("error when setting a webhook: %w", err)
	}

	updates := bot.ListenForWebhook("/")
	api := NewBotHandler(db, rc, logger)
	mw := NewMiddlewareBot(timings, counts, api)
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		for update := range updates {
			err := process(update, mw, bot)
			if err != nil {
				_, err = bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID,
					fmt.Sprintf("you entered: %s. This is not a valid request. Use /help to find out the list of valid commands",
						update.Message.Text)))
				if err != nil {
					mw.API.logger.Errorf("error when sending message by bot: %w", err)
					return
				}
			}
		}
		// здесь еще контекст зафигачить
	}(wg)

	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/state", func(w http.ResponseWriter, r *http.Request) {
		_, err = w.Write([]byte("all is working"))
		if err != nil {
			mw.API.logger.Errorf("error when sending message by response writer: %w", err)
			return
		}
	})
	port := os.Getenv("PORT")
	if port == "" {
		port = "8081"
	}

	errChServer := make(chan error)
	go func(errChServer chan error) {
		err := http.ListenAndServe(":"+port, nil)
		if err != nil {
			errChServer <- err
		}
		close(errChServer)
	}(errChServer)

	stop := time.NewTimer(50 * time.Millisecond)
	select {
	case err := <-errChServer:
		if err != nil {
			return fmt.Errorf("error when raising the server: %w", err)
		}
	case <-stop.C:
	}
	wg.Wait()

	return nil
}

func (mw *MiddlewareBot) updateStatistics(status, command string, start time.Time) {
	mw.Lock()
	mw.timings.WithLabelValues(status, command).Observe(float64(time.Since(start).Milliseconds()))
	mw.counts.WithLabelValues(status, command).Inc()
	mw.Unlock()
}

func (mw *MiddlewareBot) doRequest(query string, update tgbotapi.Update, bot *tgbotapi.BotAPI) {
	user := update.Message.From
	start := time.Now()
	switch {
	case query == "/tasks":
		mw.API.Tasks(bot, &update, user)
		mw.updateStatistics("200", "/tasks", start)
	case strings.Contains(query, "/new"):
		err := mw.API.New(bot, &update, user, query)
		if err != nil {
			mw.updateStatistics("500", "/new", start)
		} else {
			mw.updateStatistics("200", "/new", start)
		}
	case strings.Contains(query, "/assign_"):
		err := mw.API.Assign(bot, &update, user, query)
		if err != nil {
			mw.updateStatistics("500", "/assign", start)
		} else {
			mw.updateStatistics("200", "/assign", start)
		}
	case strings.Contains(query, "/unassign_"):
		err := mw.API.Unassign(bot, &update, user, query)
		if err != nil {
			mw.updateStatistics("500", "/unassign", start)
		} else {
			mw.updateStatistics("200", "/unassign", start)
		}
	case strings.Contains(query, "/resolve_"):
		err := mw.API.Resolve(bot, &update, user, query)
		if err != nil {
			mw.updateStatistics("500", "/resolve", start)
		} else {
			mw.updateStatistics("200", "/resolve", start)
		}
	case query == "/my":
		mw.API.My(bot, &update, user)
		mw.updateStatistics("200", "/my", start)
	case query == "/owner":
		mw.API.Owner(bot, &update, user)
		mw.updateStatistics("200", "/owner", start)
	case query == "/help":
		_, err := bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID,
			"Available commands:\n/tasks\n/new XXX YYY ZZZ\n/assign_ID\n/unassign_ID\n/resolve_ID\n/my\n/owner"))
		if err != nil {
			mw.API.logger.Errorf("error when sending message by bot: %w", err)
			return
		}
	default:
		_, err := bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID,
			"wrong request. You entered: "+query+
				". Available commands: /tasks, /new, /assign_ID, /unassign_ID, /resolve_ID, /my, /owner"))
		if err != nil {
			mw.API.logger.Errorf("error when sending message by bot: %w", err)
			return
		}
	}
}

func process(update tgbotapi.Update, mw *MiddlewareBot, bot *tgbotapi.BotAPI) error {
	if update.Message == nil { // TODO очень скользкий момент. Посмотреть почему prometheus не работает, как надо
		fmt.Println(1)
		return nil
	}
	user := update.Message.From
	if !mw.API.repo.CheckUserByUserName(user.UserName) {
		err := mw.API.repo.AddNewUserByID(user, user.UserName)
		if err != nil {
			mw.API.logger.Errorf("error when executing a function named `Process`, method AddNewUserByID: %w", err)
			return err
		}
		err = mw.API.repo.AddNewChatIDUsersByID(update.Message.Chat.ID, user.UserName)
		if err != nil {
			mw.API.logger.Errorf("error when executing a function named `Process`, method AddNewChatIDUsersByID: %w", err)
			return err
		}
	}
	query := update.Message.Text
	mw.doRequest(query, update, bot)
	return nil
}
