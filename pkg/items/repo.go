package items

import (
	"errors"
	"fmt"
	"task_manager/pkg/chats"
	"task_manager/pkg/tasks"

	"github.com/garyburd/redigo/redis"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const collectionName = "items"

const taskIDKey = "TaskID"

type ItemsRepo struct {
	data      DatabaseHelper
	redisConn redis.Conn
}

func NewItemsRepo(db DatabaseHelper, rc redis.Conn) *ItemsRepo {
	return &ItemsRepo{
		data:      db,
		redisConn: rc,
	}
}

func (ir ItemsRepo) GetTaskID() (uint64, error) {
	id, err := redis.Uint64(ir.redisConn.Do("GET", taskIDKey))
	if errors.Is(err, redis.ErrNil) {
		result, err := redis.String(ir.redisConn.Do("SET", taskIDKey, 1))
		if result != "OK" || err != nil {
			return 1, err
		}
		return 1, nil
	}
	if err != nil {
		return 1, err
	}
	return id, err
}

func (ir *ItemsRepo) UpdateTaskID(newID uint64) error {
	_, err := redis.String(ir.redisConn.Do("SET", taskIDKey, newID))
	return err
}

func (ir ItemsRepo) CheckTaskByID(taskID uint64) bool {
	err := ir.data.Collection(collectionName).Find(bson.M{"id": taskID}).One(&struct{}{})
	if errors.Is(err, mgo.ErrNotFound) || err != nil {
		return false
	}
	return true
}

func (ir *ItemsRepo) AddNewTaskByID(task *tasks.Task) error {
	err := ir.data.Collection(collectionName).Find(bson.M{"id": task.ID}).One(&struct{}{})
	if !errors.Is(err, mgo.ErrNotFound) {
		return fmt.Errorf("The task with the given id %d already exists. Error: %w", task.ID, err)
	}
	newTask := bson.M{
		"id":       task.ID,
		"owner":    task.Owner,
		"executor": task.Executor,
		"name":     task.Name,
	}
	err = ir.data.Collection(collectionName).Insert(newTask)
	if err != nil {
		return fmt.Errorf("error when placing a task in the database. Error: %w", err)
	}
	return nil
}

func (ir ItemsRepo) GetTaskByID(taskID uint64) (*tasks.Task, error) {
	task := &tasks.Task{}
	err := ir.data.Collection(collectionName).Find(bson.M{"id": taskID}).One(task)
	if err != nil {
		return nil, fmt.Errorf("error while getting the task from the database. error: %w", err)
	}
	return task, nil
}

func (ir *ItemsRepo) UpdateTask(task *tasks.Task) error {
	err := ir.data.Collection(collectionName).Update(
		bson.M{"id": task.ID},
		bson.M{
			"id":       task.ID,
			"owner":    task.Owner,
			"executor": task.Executor,
			"name":     task.Name,
		},
	)
	if err != nil {
		return fmt.Errorf("error while updating the task from the database. error: %w", err)
	}
	return nil
}

func (ir *ItemsRepo) DeleteTaskByID(taskID uint64) error {
	err := ir.data.Collection(collectionName).Remove(bson.M{"id": taskID})
	if err != nil {
		return fmt.Errorf("error when deleting a task from the database. error: %w", err)
	}
	return nil
}

func (ir ItemsRepo) GiveTasksWithCondition(field, pattern string) []*tasks.Task {
	result := []*tasks.Task{}
	if field != "owner" && field != "executor" {
		return result
	}
	err := ir.data.Collection(collectionName).Find(bson.M{field: pattern}).All(&result)
	if err != nil {
		return []*tasks.Task{}
	}
	return result
}

func (ir ItemsRepo) GiveTasks() []*tasks.Task {
	result := []*tasks.Task{}
	err := ir.data.Collection(collectionName).Find(bson.M{}).All(&result)
	if err != nil {
		return []*tasks.Task{}
	}
	return result
}

func (ir *ItemsRepo) AddNewChatIDUsersByID(chatID int64, userID string) error {
	err := ir.data.Collection(collectionName).Find(bson.M{"userID": userID}).One(&struct{}{})
	if err == nil {
		return nil
	}
	if !errors.Is(err, mgo.ErrNotFound) {
		return fmt.Errorf("The chat with the given user id %s already exists. Error: %w", userID, err)
	}
	err = ir.data.Collection(collectionName).Insert(bson.M{"userID": userID, "chatID": chatID})
	if err != nil {
		return fmt.Errorf("error when placing a chat in the database. Error: %w", err)
	}
	return nil
}

func (ir ItemsRepo) GetChatIDUsersByID(userID string) (int64, error) {
	chat := &chats.Chat{}
	err := ir.data.Collection(collectionName).Find(bson.M{"userID": userID}).One(chat)
	if err != nil {
		return 0, fmt.Errorf("error while getting the chat from the database. error: %w", err)
	}
	return chat.ChatID, nil
}

func (ir *ItemsRepo) AddNewUserByID(user *tgbotapi.User, userID string) error {
	err := ir.data.Collection(collectionName).Find(bson.M{"id": userID}).One(&struct{}{})
	if !errors.Is(err, mgo.ErrNotFound) {
		return fmt.Errorf("The user with the given id %s already exists. Error: %w", userID, err)
	}
	newUser := bson.M{
		"id":            user.ID,
		"first_name":    user.FirstName,
		"last_name":     user.LastName,
		"user_name":     user.UserName,
		"language_code": user.LanguageCode,
		"is_bot":        user.IsBot,
	}
	err = ir.data.Collection(collectionName).Insert(newUser)
	if err != nil {
		return fmt.Errorf("error when placing a user in the database. Error: %w", err)
	}
	return nil
}

func (ir ItemsRepo) CheckUserByUserName(userName string) bool {
	err := ir.data.Collection(collectionName).Find(bson.M{"user_name": userName}).One(&struct{}{})
	if errors.Is(err, mgo.ErrNotFound) || err != nil {
		return false
	}
	return true
}
