package users

import tgbotapi "gopkg.in/telegram-bot-api.v4"

type User struct {
	TgUser *tgbotapi.User
}
